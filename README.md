Timing attacks against RSA
==========================

Implementation of two different approaches to timing attacks against RSA.
One from Kocher, that in 1996, was the first to publish a timing attack against modular exponentiation algorithms.
The other, from Brumley and Boneh, that in 2005 published an article about how they successfully performed a remote attack against an OpenSSL server with RSA-based authentication.

### References

- Kocher, P.C., 1996, August. Timing attacks on implementations of Diffie-Hellman, RSA, DSS, and other systems. In Annual International Cryptology Conference (pp. 104-113). Springer, Berlin, Heidelberg.
- Brumley, D. and Boneh, D., 2005. Remote timing attacks are practical. Computer Networks, 48(5), pp.701-716.
- Boreale, M., 2003. Note per il corso di Sicurezza delle Reti.
- Montgomery, P.L., 1985. Modular multiplication without trial division. Mathematics of computation, 44(170), pp.519-521.
- Menezes, A.J., Van Oorschot, P.C. and Vanstone, S.A., 2018. Handbook of applied cryptography. CRC press.
- Schindler, W., 2000, August. A timing attack against RSA with the chinese remainder theorem. In International Workshop on Cryptographic Hardware and Embedded Systems (pp. 109-124). Springer, Berlin, Heidelberg.
- Coppersmith, D., 1997. Small solutions to polynomial equations, and low exponent RSA vulnerabilities. Journal of cryptology, 10(4), pp.233-260.

